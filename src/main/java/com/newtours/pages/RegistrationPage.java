package com.newtours.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.NAME, using = "firstName")
    private WebElement firstNameTextBox;

    @FindBy(name = "lastName")
    private WebElement lastNameTextBox;

    @FindBy(how = How.NAME, using = "email")
    private WebElement usernameTextBox;

    @FindBy(how = How.NAME, using = "password")
    private WebElement passwordTextBox;

    @FindBy(how = How.NAME, using = "confirmPassword")
    private WebElement confirmPasswordTextBox;

    @FindBy(how = How.NAME, using = "submit")
    private WebElement submitBtn;

    public RegistrationPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void goTo() {
        driver.get("http://demo.guru99.com/test/newtours/register.php");
        wait.until(ExpectedConditions.visibilityOf(firstNameTextBox));
    }

    public void enterUserDetails(String firstName, String lastName){
        firstNameTextBox.sendKeys(firstName);
        lastNameTextBox.sendKeys(lastName);
    }

    public void enterUserCredentials(String username, String password){
        usernameTextBox.sendKeys(username);
        passwordTextBox.sendKeys(password);
        confirmPasswordTextBox.sendKeys(password);
    }

    public void submit(){
        submitBtn.click();
    }
}
