package com.newtours.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FindFlightPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.CSS, using = "a > img[src='images/home.gif']")
    private WebElement backToHomeBtn;

    @FindBy(css = "td > img[src='images/hdr_findflight.gif']")
    private WebElement findAFlightImage;

    @FindBy(css = "td > img[src='images/hdr_specials.gif']")
    private WebElement specialsWidget;

    @FindBy(css = "table > tbody > tr:nth-child(2) > td:nth-child(1) > table:nth-child(2)")
    private WebElement priceTable;

    public FindFlightPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void backToHome(){
        this.wait.until(ExpectedConditions.visibilityOf(backToHomeBtn));
        this.backToHomeBtn.click();
        wait.until(ExpectedConditions.visibilityOf(findAFlightImage));
        Boolean isDisplayed = findAFlightImage.isDisplayed();
        System.out.println(isDisplayed);
    }

    public void getListOfPrices(){
        wait.until(ExpectedConditions.visibilityOf(specialsWidget));
        List<WebElement> priceList = priceTable.findElements(By.cssSelector("div[align='right'] font"));
        for(WebElement d_prices : priceList ) {
            String price = d_prices.getText();
            System.out.println(price);
        }
    }
}
