package com.searchmodule.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(how = How.NAME, using = "q")
    private WebElement searchBox;

    @FindBy(how = How.ID, using = "search_button_homepage")
    private WebElement searchIcon;

    @FindBy(how = How.LINK_TEXT, using = "Videos")
    private WebElement videosLink;

    @FindBy(css = "div[class*='tile--vid']")
    private List<WebElement> allVideos;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,30);
        PageFactory.initElements(driver, this);
    }

    public void goTo(){
        driver.get("https://duckduckgo.com/");
    }

    public void searchKeyword(String keyword){
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.sendKeys(keyword);
        searchIcon.click();
    }

    public void goToVideos(){
        wait.until(ExpectedConditions.elementToBeClickable(videosLink));
        videosLink.click();
    }

    public int printResult(){
        By by = By.cssSelector("div[class*='tile--vid']");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(by,0));
        return allVideos.size();
    }
}
