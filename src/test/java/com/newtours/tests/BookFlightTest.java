package com.newtours.tests;

import com.newtours.pages.FindFlightPage;
import com.newtours.pages.FlightDetailsPage;
import com.newtours.pages.RegistrationConfirmationPage;
import com.newtours.pages.RegistrationPage;
import com.tests.BaseTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class BookFlightTest extends BaseTest {

    @Test
    @Parameters({"username"})
    public void registrationPageTest(String username){
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.goTo();
        registrationPage.enterUserDetails("firstUserName", "lastTestName");
        registrationPage.enterUserCredentials(username, "firstPassword");
        registrationPage.submit();
    }

    @Test(dependsOnMethods = "registrationPageTest")
    public void registrationConfirmationTest(){
        RegistrationConfirmationPage registrationConfirmationPage = new RegistrationConfirmationPage(driver);
        registrationConfirmationPage.goToFlightDetailsPage();
    }

    @Test(dependsOnMethods = "registrationConfirmationTest")
    @Parameters({"numberOfPassengers"})
    public void flightDetailsTest(String numberOfPassengers){
        FlightDetailsPage flightDetailsPage = new FlightDetailsPage(driver);
        flightDetailsPage.selectPassengers(numberOfPassengers);
        flightDetailsPage.goToFindFlightsPage();
    }

    @Test(dependsOnMethods = "flightDetailsTest")
    public void findFlightTest(){
        FindFlightPage findFlightPage = new FindFlightPage(driver);
        findFlightPage.backToHome();
        // homepage.getListOfPrices()
        findFlightPage.getListOfPrices();
    }
}
