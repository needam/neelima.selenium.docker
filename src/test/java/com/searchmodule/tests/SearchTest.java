package com.searchmodule.tests;

import com.searchmodule.pages.SearchPage;
import com.tests.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SearchTest extends BaseTest {

    @Test
    @Parameters({"keyword"})
    public void searchFor(String keyword){
        SearchPage searchPage =new SearchPage(driver);
        searchPage.goTo();
        searchPage.searchKeyword(keyword);
        searchPage.goToVideos();
        int videosCount = searchPage.printResult();
        Assert.assertTrue(videosCount>0);
    }
}
