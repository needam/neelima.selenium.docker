FROM openjdk:8u191-jre-alpine3.8

RUN apk add curl jq

# Workspace
WORKDIR user/share/neelima

#Add .jar from target host into this image
ADD target/neelima-selenium-docker.jar           neelima-selenium-docker.jar
ADD target/neelima-selenium-docker-tests.jar     neelima-selenium-docker-tests.jar
ADD target/libs                                  libs

#can add other dependencies as well here .csv json xls
# add suite files 
ADD book-flight-module.xml                       book-flight-module.xml
ADD search-module.xml                            search-module.xml

# ADD health check script
ADD healthcheck.sh                               healthcheck.sh
RUN dos2unix healthcheck.sh

# BROWSER
# HUB_HOST
# MODULE
ENTRYPOINT sh healthcheck.sh
